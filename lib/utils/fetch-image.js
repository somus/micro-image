const crypto = require('crypto');
const fetch = require('node-fetch');
const normalizeUrl = require('normalize-url');
const debug = require('debug')('micro-image:fetch');

const { getImageFromCache, saveImageToCache } = require('./cache-utils');

/**
 * Creates a hashed filename for the given image url. Also adds image extemsion to the filename.
 * @param  {String} imageUrl Image URL
 * @return {String}          hashed filename for the given url
 */
const getFilename = imageUrl => {
	const fileExtension = imageUrl.split('.').pop();
	const fileHash = crypto.createHash('md5').update(imageUrl).digest('hex');

	return `${fileHash}.${fileExtension}`;
};

/**
 * Fetches the image from cache / downloads it.
 * @param  {String} imageUrl Image URL
 * @return {Buffer}          Image buffer data
 */
module.exports = async imageUrl => {
	const normalizedUrl = normalizeUrl(imageUrl);

	debug(`fetching image - ${normalizedUrl}`);

	const imageFileName = getFilename(normalizedUrl);
	const cachedImage = getImageFromCache(imageFileName);

	if (cachedImage) {
		return cachedImage;
	}

	const imageBuffer = await fetch(normalizedUrl).then(res => res.buffer());

	saveImageToCache(imageFileName, imageBuffer);

	return { imageFileName, imageBuffer };
};
