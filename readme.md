# micro-image

A micro service to process images on the fly

_This is a WIP. Not in a usable state._

## Start the application
Run the following commands to start the application
```
git clone https://github.com/somus/micro-image.git
cd micro-image
yarn
yarn start
```

## Working routes
`/info` - displays the Exif information of the provided image. The following url
```
http://localhost:5000/info?url=https://cdn.zapier.com/static/1DN7DJ/images/frontend/onboarding/zapier-small-orange-logo.png
```
will return
```
{
	"format":"png",
	"width":126,
	"height":58,
	"space":"srgb",
	"channels":4,
	"depth":"uchar",
	"density":72,
	"hasProfile":false,
	"hasAlpha":true
}
```

