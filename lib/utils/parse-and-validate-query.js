const { sendError } = require('micro');
const qs = require('query-string');
const Joi = require('joi');
const debug = require('debug')('micro-image:validate');

const DEV = process.env.NODE_ENV === 'development';

/**
 * Parses an url and returns the query data as an object
 * @param  {String} url URL to be parsed
 * @return {Object}     Query data object
 */
const parseQuery = url => qs.parse(qs.extract(url));

const parseOptions = options => {
	const optionsObject = options.split(',').reduce((optionsObject, option) => {
		const [name, value] = option.split('_');

		if (!name) return optionsObject;

		optionsObject[name] = value;
		return optionsObject;
	}, {});

	return Object.keys(optionsObject).length > 0 ? optionsObject : null;
};

/**
 * Validates the given request against the schema
 * @param  {http.ClientRequest} req    Request object
 * @param  {http.ServerResponse} res    Response object
 * @param  {Joi.Object} schema Joi validation schema
 * @return {null|String}        query params from the reqest
 * @throws {Error} If schema param is not present
 */
module.exports = (req, res, schema) => {
	if (!schema) {
		throw new Error('joi schema required.');
	}

	const query = parseQuery(req.url);
	if (query.options && query.options.trim() !== '') {
		const parsedOptions = parseOptions(query.options);

		if (parsedOptions) {
			query.options = parsedOptions;
		}
	}

	debug(`validating query - ${JSON.stringify(query)}`);

	const error = Joi.validate(query, schema, { allowUnknown: true }).error;

	if (error) {
		debug(`validating failed - ${JSON.stringify(query)}`);

		error.statusCode = 400;
		if (!DEV) {
			error.message = 'Invalid request';
		}
		sendError(req, res, error);
		return null;
	}

	return query;
};
