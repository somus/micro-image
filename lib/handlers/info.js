const { send } = require('micro');
const Joi = require('joi');
const sharp = require('sharp');

const query = require('../utils/parse-and-validate-query');
const fetchImage = require('../utils/fetch-image');

const validationSchema = Joi.object({
	url: Joi.string().uri({ scheme: /https?/ }).required(),
});

/**
 * /info route handler
 * @param  {http.clientRequest} req Request object
 * @param  {http.ServerResponse} res Response object
 * @return {null}
 */
module.exports = async (req, res) => {
	const queryParams = query(req, res, validationSchema);

	if (!queryParams) return null;

	const { imageBuffer } = await fetchImage(queryParams.url);
	const image = sharp(imageBuffer);
	const metadata = await image.metadata();

	res.setHeader('Cache-Control', 'max-age=31536000, public, s-maxage=31536000');
	res.setHeader('Expires', new Date(Date.now() + 31536000000).toUTCString());

	send(res, 200, metadata);
	return null;
};
