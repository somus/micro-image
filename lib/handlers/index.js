const rootHandler = require('./root');
const infoHandler = require('./info');
const transformHandler = require('./transform');

module.exports = {
	rootHandler,
	infoHandler,
	transformHandler,
};
