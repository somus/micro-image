const fs = require('fs');
const debug = require('debug')('micro-image:cache');

/**
 * Checks whether the given file is in cache
 * @param  {String} filename Filename
 * @return {Bool}
 */
const isCached = filename => {
	debug(`checking for existing cache - ${filename}`);
	return fs.existsSync(`./images/${filename}`);
};

/**
 * Gets image from cache from present
 * @param  {String} filename Image filename
 * @return {null|Buffer}          Image buffer data
 */
const getImageFromCache = filename => {
	if (!isCached(filename)) return null;

	debug(`retrieving from cache - ${filename}`);
	return fs.readFileSync(`./images/${filename}`);
};

/**
 * Saves an image to cache
 * @param  {String} filename    Filename for the image to be saved
 * @param  {Buffer} imageBuffer Image buffer data
 */
const saveImageToCache = async (filename, imageBuffer) => {
	debug(`saving to cache - ${filename}`);
	fs.writeFileSync(`./images/${filename}`, imageBuffer);
};

module.exports = {
	isCached,
	getImageFromCache,
	saveImageToCache,
};
