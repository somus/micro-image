const fs = require('fs');
const { send } = require('micro');
const { router, get } = require('microrouter');
const debug = require('debug')('micro-image:bootstrap');

// Import route handlers
const {
	rootHandler,
	infoHandler,
	transformHandler,
} = require('./lib/handlers');

debug(`booting in ${process.env.NODE_ENV} mode`);

// Create images folder if not present
debug('checking if ./images folder exists');
if (!fs.existsSync('./images')) {
	fs.mkdirSync('./images');
	debug('created ./images folder');
}

const notFoundHandler = (req, res) => send(res, 404, 'Not found');

module.exports = router(
	get('/', rootHandler),
	get('/info', infoHandler),
	get('/transform', transformHandler),
	get('/*', notFoundHandler)
);
