const micro = require('micro');
const test = require('ava');
const listen = require('test-listen');
const request = require('request-promise');
const app = require('..');

test('Main endpoint', async t => {
	const service = micro(app);

	const url = await listen(service);
	const body = await request(url);

	t.deepEqual(body, 'Hello World!');
});
